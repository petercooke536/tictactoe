import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;

public class Board implements Comparable<Board>{

	public ArrayList<PositionObject> positions = new ArrayList<PositionObject>();
	private int countX;
	private int countO;
	
	public Board(String s) {	
		positions.add(new PositionObject(new Rectangle(140, 140, 190, 190)));
		positions.add(new PositionObject(new Rectangle(350, 140, 190, 190)));
		positions.add(new PositionObject(new Rectangle(560, 140, 190, 190)));
		positions.add(new PositionObject(new Rectangle(140, 350, 190, 190)));
		positions.add(new PositionObject(new Rectangle(350, 350, 190, 190)));
		positions.add(new PositionObject(new Rectangle(560, 350, 190, 190)));
		positions.add(new PositionObject(new Rectangle(140, 560, 190, 190)));
		positions.add(new PositionObject(new Rectangle(350, 560, 190, 190)));
		positions.add(new PositionObject(new Rectangle(560, 560, 190, 190)));
		
		countX = 0;
		countO = 0;
	}
	
	/**
	 * Checks if game is over
	 * @return
	 */
	public boolean isGameOver() {
		return (hasWon() || isDraw());
	}
	
	
	
	/**
	 * Checks if the point is a valid spot to place a X or O on the board
	 * @param p Point of where player clicked
	 * @return The positions index, -1 if not found
	 */
	public int getBoardPosition(Point p) {
		
		for(int i = 0; i < positions.size(); i++) 
			if(positions.get(i).getSelectionArea().contains(p))
				return i;
		return -1;
	}
	
	/**
	 * Places at point p
	 * @param p Point from GUI
	 * @return true if can place
	 */
	public boolean place(Point p) {
		int pos = getBoardPosition(p);
		if(pos != -1 && positions.get(pos).isEmpty()) {
			if(countX <= countO) {
				positions.get(pos).setX();
				countX++;
			}else {
				positions.get(pos).setO();
				countO++;
			}
			return true;
		}
		return false;
	}
	
	/**
	 * Places in position 0-8
	 * @param pos 0-8 starting in top left as 0
	 * @return true if placed
	 */
	public boolean place(int pos) {
		positions.get(pos).setO();
		countO++;
		return true;
	}
	
	/**
	 * Resets the board
	 */
	public void reset() {
		countX = 0;
		countO = 0;
		for(PositionObject p : positions) 
			p.reset();
		
		
	}
	/**
	 * Checks is someone has won
	 * @return true if someone has won
	 */
	public boolean hasWon() {
		if(this.positions.get(0).equals(this.positions.get(1)) && this.positions.get(1).equals(this.positions.get(2))
				|| 	this.positions.get(3).equals(this.positions.get(4)) && this.positions.get(3).equals(this.positions.get(5))
				||  this.positions.get(6).equals(this.positions.get(7)) && this.positions.get(6).equals(this.positions.get(8))
				||  this.positions.get(0).equals(this.positions.get(3)) && this.positions.get(0).equals(this.positions.get(6))
				||  this.positions.get(1).equals(this.positions.get(4)) && this.positions.get(1).equals(this.positions.get(7))
				||  this.positions.get(2).equals(this.positions.get(5)) && this.positions.get(2).equals(this.positions.get(8))
				||  this.positions.get(0).equals(this.positions.get(4)) && this.positions.get(0).equals(this.positions.get(8))
				||  this.positions.get(2).equals(this.positions.get(4)) && this.positions.get(2).equals(this.positions.get(6))
				){
					return true;
			}
		return false;
	}
	
	/**
	 * Checks if game is a draw
	 * @return true if game is a draw
	 */
	public boolean isDraw() {
		if ((countX + countO) == 9)
			return true;
		return false;
	}
	
	/**
	 * Gets the number of X's on the board
	 * @return the number of X's
	 */
	public int getXCount() {
		return countX;
	}
	
	/**
	 * Gets the number of O's on the board
	 * @return the number of O's
	 */
	public int getOCount() {
		return countO;
	}
	
	@Override
	public String toString() {
		String s = "";
		for(int x = 0; x < positions.size(); x ++) {
			if(positions.get(x).isEmpty()) {
				s += ""+x;
			}else {
				s += positions.get(x).toString();
			}
		}		
		return s;
	}
	
	@Override
	public int hashCode() {
		return super.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		return this.toString().equals(obj.toString());
	}

	@Override
	public int compareTo(Board b) {
		return this.toString().compareTo(b.toString());
	}
	
	
	
}
