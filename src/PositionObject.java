import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

public class PositionObject {

	private boolean isEmpty;
	private boolean isX;
	private boolean isO;
	private Rectangle area;
	private BufferedImage xImage = null;
	private BufferedImage oImage = null;
	
	
	public PositionObject(Rectangle r) {
		area = r;
		isEmpty = true;
		isX = false;
		isO = false;
		xImage = GameInteraction.loadImage("X_TTT.png");
		oImage = GameInteraction.loadImage("O_TTT.png");
	}
	
	/**
	 * Resets the PositionObject
	 */
	public void reset() {
		isEmpty = true;
		isX = false;
		isO = false;
	}
	
	/**
	 * Sets the Object to X
	 */
	public void setX() {
		isX = true;
		isEmpty = false;
		isO = false;
	}
	
	/**
	 * Sets the Object to O
	 */
	public void setO() {
		isO = true;
		isEmpty = false;
		isX = false;
	}
	
	/**
	 * Sets the Object to Empty
	 */
	public void setEmpty() {
		isEmpty = true;
		isO = false;
		isX = false;
	}
	
	/**
	 * Checks if the position is empty
	 * @return return true if empty
	 */
	public boolean isEmpty() {
		return isEmpty;
	}
	
	/**
	 * Checks if the position is X
	 * @return return true if X
	 */
	public boolean isX() {
		return isX;
	}
	
	/**
	 * Checks if the position is O
	 * @return return true if O
	 */
	public boolean isO() {
		return isO;
	}
	
	/**
	 * Gets the selection area on screen
	 * @return selection area
	 */
	public Rectangle getSelectionArea() {
		return area;
	}
	
	/**
	 * Draws the image.
	 * @param g
	 */
	public void draw(Graphics g) {
		if(isX) {
			g.drawImage(xImage, area.x, area.y, null);
		}else if(isO) {
			g.drawImage(oImage, area.x, area.y, null);
		}
	}
	
	@Override
	public String toString() {
		if(isEmpty)
			return "-";
		if(isX)
			return "X";
		if(isO)
			return "O";
		
		return "";
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof PositionObject) {
			PositionObject po = (PositionObject) obj;
			if((this.isO && po.isO) || (this.isX && po.isX))
				return true;
		}
		return false;
	}
}
