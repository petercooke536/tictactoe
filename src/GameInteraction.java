import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class GameInteraction extends JPanel {

	public static JPanel canvas;
	public JFrame window = new JFrame("TicTacToe");
	public Toolkit tk = Toolkit.getDefaultToolkit();
	public Board board = null;
	public Game game = null;
	public BufferedImage bgImage = null;
	public BufferedImage winImage = null;
	public BufferedImage loseImage = null;
	public BufferedImage drawImage = null;

	private boolean test = false;

	public GameInteraction() {
		canvas = this;

		board = new Board("---------");
		game = new Game(board);

		bgImage = loadImage("background.png");
		drawImage = loadImage("Draw.png");
		winImage = loadImage("Winner.png");
		loseImage = loadImage("Loser.png");

		window.setBounds(tk.getScreenSize().width / 2 - 250, tk.getScreenSize().height / 2 - 400, 800, 800);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setAlwaysOnTop(true);
		window.add(this);
		window.setVisible(true);
		window.setFocusable(true);
		window.setResizable(false);
		window.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent e) {
				if (!board.isGameOver()) {
					if (board.place(e.getPoint())) {
						/*
						if (board.isGameOver()) {
							repaint();
							return;
						}
						*/
						// Computers turn to play
						if(!board.isGameOver()) {
							int move = game.getBestMove();
							board.place(move);
						}
						repaint();
					}
				} else {
					game.reset();
					board.reset();
					repaint();
				}
			}

			@Override
			public void mouseReleased(MouseEvent e) {
			}

			@Override
			public void mousePressed(MouseEvent e) {
			}

			@Override
			public void mouseExited(MouseEvent e) {
			}

			@Override
			public void mouseEntered(MouseEvent e) {
			}

		});

	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(bgImage, 0, 0, null);
		for (int i = 0; i < board.positions.size(); i++) {
			board.positions.get(i).draw(g);
		}
		if (board.isDraw()) {
			g.drawImage(drawImage, 0, 0, null);
		} else if (board.isGameOver() && board.getOCount() == board.getXCount()) {
			g.drawImage(loseImage, 0, 0, null);
		}

	}

	public static BufferedImage loadImage(String i) {
		BufferedImage image = null;
		URL resource = GameInteraction.class.getResource(i);
		try {
			image = ImageIO.read(resource);
			return image;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void main(String[] args) {
		new GameInteraction();
	}

}
