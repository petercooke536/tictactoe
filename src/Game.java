
public class Game {

	private Board board;
	private boolean isOver, isDraw;
	private SortedArrayDictionary<String, Integer> dictionary = new SortedArrayDictionary<String, Integer>();

	public Game(Board b) {
		board = b;
		isOver = false;
		isDraw = false;

		generateDictionary();

	}
	
	/**
	 * Generates the dictionary as the board as the key and best move as value
	 */
	private void generateDictionary() {
		for (int a = 0; a < 3; a++)
			for (int b = 0; b < 3; b++)
				for (int c = 0; c < 3; c++)
					for (int d = 0; d < 3; d++)
						for (int e = 0; e < 3; e++)
							for (int f = 0; f < 3; f++)
								for (int g = 0; g < 3; g++)
									for (int h = 0; h < 3; h++)
										for (int i = 0; i < 3; i++) {

											int x = 0; // X = 1
											int o = 0; // O = 2

											x += (a == 1 ? 1 : 0);
											x += (b == 1 ? 1 : 0);
											x += (c == 1 ? 1 : 0);
											x += (d == 1 ? 1 : 0);
											x += (e == 1 ? 1 : 0);
											x += (f == 1 ? 1 : 0);
											x += (g == 1 ? 1 : 0);
											x += (h == 1 ? 1 : 0);
											x += (i == 1 ? 1 : 0);

											o += (a == 2 ? 1 : 0);
											o += (b == 2 ? 1 : 0);
											o += (c == 2 ? 1 : 0);
											o += (d == 2 ? 1 : 0);
											o += (e == 2 ? 1 : 0);
											o += (f == 2 ? 1 : 0);
											o += (g == 2 ? 1 : 0);
											o += (h == 2 ? 1 : 0);
											o += (i == 2 ? 1 : 0);

											// Is vaild
											// Is x >= 0 count
											if (x >= o) {
												String boardString = setValue(a, 0) + setValue(b, 1) + setValue(c, 2)
														+ setValue(d, 3) + setValue(e, 4) + setValue(f, 5)
														+ setValue(g, 6) + setValue(h, 7) + setValue(i, 8);

												System.out.println(boardString);
												dictionary.add(boardString, generateBestMove(boardString));
											}

										}

	}

	/**
	 * Generates the best move given board
	 * @param s board
	 * @return index of best move for board
	 */
	private int generateBestMove(String s) {
		String[] stringArray = s.split("");
		int xCount = 0, oCount = 0;
		for (int i = 0; i < stringArray.length; i++) {
			if (stringArray[i].equals("X")) {
				xCount++;
			} else if (stringArray[i].equals("O")) {
				oCount++;
			}
		}

		// Check if center spot is open
		if (stringArray[4].equals("4"))
			return 4;

		// Check rows
		if (stringArray[0].equals(stringArray[1]) && stringArray[2].equals("2"))
			return 2;
		if (stringArray[1].equals(stringArray[2]) && stringArray[0].equals("0"))
			return 0;
		if (stringArray[0].equals(stringArray[2]) && stringArray[1].equals("1"))
			return 1;
		if (stringArray[3].equals(stringArray[4]) && stringArray[5].equals("5"))
			return 5;
		if (stringArray[4].equals(stringArray[5]) && stringArray[3].equals("3"))
			return 3;
		if (stringArray[3].equals(stringArray[5]) && stringArray[4].equals("4"))
			return 4;
		if (stringArray[6].equals(stringArray[7]) && stringArray[8].equals("8"))
			return 8;
		if (stringArray[7].equals(stringArray[8]) && stringArray[6].equals("6"))
			return 6;
		if (stringArray[6].equals(stringArray[8]) && stringArray[7].equals("7"))
			return 7;

		// Check cols
		if (stringArray[0].equals(stringArray[3]) && stringArray[6].equals("6"))
			return 6;
		if (stringArray[3].equals(stringArray[6]) && stringArray[0].equals("0"))
			return 0;
		if (stringArray[0].equals(stringArray[6]) && stringArray[3].equals("3"))
			return 3;
		if (stringArray[1].equals(stringArray[4]) && stringArray[7].equals("7"))
			return 7;
		if (stringArray[4].equals(stringArray[7]) && stringArray[1].equals("1"))
			return 1;
		if (stringArray[1].equals(stringArray[7]) && stringArray[4].equals("4"))
			return 0;
		if (stringArray[2].equals(stringArray[5]) && stringArray[8].equals("8"))
			return 8;
		if (stringArray[5].equals(stringArray[8]) && stringArray[2].equals("2"))
			return 2;
		if (stringArray[2].equals(stringArray[8]) && stringArray[5].equals("5"))
			return 5;

		// Diagonal
		if (stringArray[6].equals(stringArray[4]) && stringArray[2].equals("2"))
			return 2;
		if (stringArray[2].equals(stringArray[4]) && stringArray[6].equals("6"))
			return 6;
		if (stringArray[6].equals(stringArray[2]) && stringArray[4].equals("4"))
			return 4;
		if (stringArray[0].equals(stringArray[4]) && stringArray[8].equals("8"))
			return 8;
		if (stringArray[4].equals(stringArray[8]) && stringArray[0].equals("0"))
			return 0;
		if (stringArray[0].equals(stringArray[8]) && stringArray[4].equals("4"))
			return 4;

		// Check for set ups
		
		if (stringArray[0].equals(stringArray[8]) && !stringArray[4].equals("4")
				&& !stringArray[4].equals(stringArray[0])) {
			if (xCount + oCount <= 3)
				return 1;
		}

		if (stringArray[2].equals(stringArray[6]) && !stringArray[4].equals("4")
				&& !stringArray[4].equals(stringArray[0])) {
			if (xCount + oCount <= 3)
				return 1;
		}

		// Check corners
		if (stringArray[0].equals("0"))
			return 0;
		if (stringArray[2].equals("2"))
			return 2;
		if (stringArray[6].equals("6"))
			return 6;
		if (stringArray[8].equals("8"))
			return 8;

		return -1;

	}
	/**
	 * Helps create the board string from the generateDictionary method
	 * @param s int to be converted into string
	 * @param index of the value
	 * @return string
	 */
	private String setValue(int s, int index) {
		if (s == 0)
			return "" + index;
		if (s == 1)
			return "X";
		if (s == 2)
			return "O";
		return "" + index;

	}

	/**
	 * Gets best move from dictionary
	 * @return best move
	 */
	public int getBestMove() {
		return dictionary.getValue(board.toString());
	}
	
	/**
	 * Checks if there is a win on the board
	 * @return true if win, false otherwise
	 */
	public boolean checkWin() {
		if (board.positions.get(0).equals(board.positions.get(1))
				&& board.positions.get(1).equals(board.positions.get(2))
				|| board.positions.get(3).equals(board.positions.get(4))
						&& board.positions.get(3).equals(board.positions.get(5))
				|| board.positions.get(6).equals(board.positions.get(7))
						&& board.positions.get(6).equals(board.positions.get(8))
				|| board.positions.get(0).equals(board.positions.get(3))
						&& board.positions.get(0).equals(board.positions.get(6))
				|| board.positions.get(1).equals(board.positions.get(4))
						&& board.positions.get(1).equals(board.positions.get(7))
				|| board.positions.get(2).equals(board.positions.get(5))
						&& board.positions.get(2).equals(board.positions.get(8))
				|| board.positions.get(0).equals(board.positions.get(4))
						&& board.positions.get(0).equals(board.positions.get(8))
				|| board.positions.get(2).equals(board.positions.get(4))
						&& board.positions.get(2).equals(board.positions.get(6))) {
			isOver = true;
			return true;
		}

		if (board.isDraw()) {
			isDraw = true;
			isOver = true;
			return true;
		}
		return false;
	}
	/**
	 * Checks if game is over
	 * @return true if game is over
	 */
	public boolean isOver() {
		return isOver;
	}

	/**
	 * Checks if game is a draw
	 * @return true if game is a draw
	 */
	public boolean isDraw() {
		return isDraw;
	}

	/**
	 * resets the game
	 */
	public void reset() {
		isOver = false;
		isDraw = false;
	}

}
